**About the Project**
****
The project is about investigating the IT field in Kazakhstan. There were several research questions to get most about this area. For example, "What are the min, max and average salaries in the IT field?". Getting answers for the questions we would like to show interesting facts about our specialty. Good Luck!

**Contents**
****
- The _"create_dataset.ipynb"_ file contains the source code that retrieves and obtain data from the "https://hh.kz". 
- The _"Mean_Max_Min.ipynb"_ file contains the source code that answers research questions "What is the average salary of the IT vacancy in Kazakhstan?" and “What is the min or max salary in an IT job?”.
- The CSV file _"vacancies.csv"_ contains the parsing data from Head Hunter as a structured form.
- The _"popular_state.ipynb"_ file contains the source code that shows a popular city in the IT field.

**Note:** Only the IT jobs are represented in the "vacancies.csv" file 